[Home page](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Homepage.md) |
[Terminology](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Terminology.md) |
[Project Idea](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Project_Idea.md)

# Heart Monitoring System
### Our STEM project for the year 2024, is to make a heart rate monitoring system:
Plan:
First we’ll work on tinkercad by creating a working model.
Then through tinkercad, we’ll make the project in real life and use its real life applications.

#### Reasons why we chose this project: 
Our project focuses on monitoring heart rate to keep our hearts healthy. By tracking heart rate regularly, we can understand our cardiovascular health, track fitness levels, and spot potential issues. This helps individuals set goals during activities like morning exercise, aiming to optimize heart rate for fat burning and reducing heart disease risks.

By monitoring heart rates during exercise, we can avoid overtraining, dehydration, and heat exhaustion. This project can also aid in stress management and reducing depression rates among students. Additionally, as many students struggle with sleep, the heart rate monitor can double as a sleep monitoring system, providing insights into our sleep patterns. 

#### Things we need:
Arduino Board:  Any Arduino board such as Arduino Uno, Arduino Nano, or Arduino Mega will work for this project.

![](https://eeeproject.b-cdn.net/wp-content/uploads/2017/06/Arduino-Uno-board-pins-description.jpg)



Heart Rate Sensor Module: You'll need a heart rate sensor module compatible with Arduino. One commonly used sensor is the Pulse Sensor (PulseSensor.com), which includes an infrared LED and photodetector for measuring heart rate.

![](https://robocraze.com/cdn/shop/products/1_1_8711c087-d290-472f-9b2c-8af431b1b83f.jpg?v=1700214749&width=1445)

Connecting Wires: Jumper wires or breadboard wires to establish connections between the Arduino board and the sensor module.

![](https://blog.sparkfuneducation.com/hubfs/EDU/BLOG%20Images/Jan%202018/JumperWire-Male-01-L.jpg)


Power Source: A power source for the Arduino board, such as a USB cable connected to a computer or a power adapter.

![](https://5.imimg.com/data5/SELLER/Default/2022/7/KX/WA/XO/98847066/uno-cable.jpg)


#### Optional Components:
Breadboard: If you prefer to prototype the circuit on a breadboard.

![](https://cdn-learn.adafruit.com/guides/images/000/001/436/medium800thumb/halfbb_640px.gif)


LEDs and Resistors: Optional for adding visual indicators or feedback based on heart rate readings.

![](https://www.rcpano.net/wp-content/uploads/2023/02/Led-Resistor-Calculater-Single-1.jpg)

Display Module: Optional for displaying heart rate readings, such as an LCD display or OLED display.

![](https://5.imimg.com/data5/SELLER/Default/2023/12/371364416/HT/ZS/WY/562456/liquid-crystal-display-module-500x500.webp)


#### Final project Design:
![alt text](photo.jpeg)

[Home page](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Homepage.md) |
[Terminology](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Terminology.md) |
[Project Idea](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Project_Idea.md)


