[Home page](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Homepage.md) |
[Terminology](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Terminology.md) |
[Project Idea](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Project_Idea.md)

# The Learning Process
We tried the lcd simulation in the website wokwi and reffered chatgpt to generate the code. 

LCD Simulation:

![!\[Alt text\](<Screenshot 2024-06-10 124755.png>)](<images/Screenshot 2024-06-10 124755.png>)

We also tried the Led simulation.
![
!\[Alt text\](<Screenshot 2024-06-10 125122.png>)](<images/Screenshot 2024-06-10 125122.png>)

This is a simulation that we did on Wokwi. Here we connected the LED and the LCD screen to the arduino uno board. So, the timing of the LED goes hand in hand with the words screening on the monitor. As in, the LED and the texts show up at the same time and goes off at the same time. We could not connect the pulse sensor because the component was not there in the website.

![!\[Alt text\](<Screenshot 2024-06-13 143048.png>)](<images/Screenshot 2024-06-13 143048.png>)

We have again created a simulation where we can make the texts scroll.

![alt text](<images/simulation copy.png>)

[Home page](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Homepage.md) |
[Terminology](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Terminology.md) |
[Project Idea](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Project_Idea.md)