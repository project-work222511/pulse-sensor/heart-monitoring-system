[Home page](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Homepage.md) |
[Terminology](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Terminology.md) |
[Project Idea](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Project_Idea.md)

# Heart Monitoring System
### Introduction:
Our project, the Heart Monitoring System, aims to improve our day-to-day health by tracking our pulse. It's vital in modern healthcare and uses components like a pulse sensor and Arduino board. At The Royal Academy, where movement is constant, monitoring our pulse is crucial. Physical activity and stress can affect heart rate, making our system valuable for keeping tabs on our health.

For our inspiration, we were looking for a proper solution for our problem, then we came across a video that inspired us and we were excited to modify and connect our ideas in the project.
The link of the video:

https://youtu.be/1LqBvkHTJXU?si=EUUpJNAZqp-azmPH

In the end, we expect our project to monitor our heart rates. We want it to be useful for everyone in the BHU, in our school campus especially during physical sessions. Our project could also be useful in learning experiences for life science or biology. In conclusion, we want our project to help people with or without heart problems.

[Home page](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Homepage.md) |
[Terminology](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Terminology.md) |
[Project Idea](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Project_Idea.md)