[Home page](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Homepage.md) |
[Terminology](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Terminology.md) |
[Project Idea](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Project_Idea.md)


Pulse sensor: When the heart pumps blood, and a detector that monitors this volume change is called a pulse sensor.

Arduino UNO: Arduino UNO is a low-cost, flexible, and easy-to-use programmable open-source microcontroller board that can be integrated into a variety of electronic projects. 

LCD display/screen: A flat-panel display or other electronically modulated optical device that uses the light-modulating properties 

Breadboard: A thin plastic board used to hold electronic components (transistors, resistors, chips, etc.) that are wired together.

LED: A light-emitting diode (a semiconductor diode which glows when a voltage is applied).

REFERENCES OR SOFTWARES USED:
-https://www.arduino.cc/
-Dr.DuinoBlog
https://lastminuteengineers.com/pulse-sensor-arduino-tutorial/

[Home page](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Homepage%20(index.md)?ref_type=heads)
| [Project Idea](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Project_Idea.md?ref_type=heads)
| [Team Members](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Team%20Information.md)
